// pigeon.js  color -- #8F3D --

var express = require('express');
var app = express();
var mongoose = require('mongoose'),
	cookieParser = require('cookie-parser'),
	flash = require('connect-flash'),
	morgan = require('morgan'),
	bodyParser = require('body-parser'),
	session = require('express-session'),
	swig = require('swig'),
	db = require('./config/db'),
	routes = require('./app/routes');

// database
mongoose.connect(db.url, function(err){
	if(err)
		throw(err)
	console.log(' connected to dabase');
});

// port
port = process.env.PORT || 3000;

// routes
var router = express.Router();
routes(app, router);

//template engine
app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', __dirname + '/views');
app.set('view cache', false);
swig.setDefaults({ cache: false });

// middleware
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ 'extended': 'true' }));
app.use(bodyParser.json());

//logging
app.use(morgan('dev'));

// 404 errors
app.use(function(req, res, next){
	res.status(404);
	res.send('404 - Not Found');
});

// 500 errors
app.use(function(err, req, res, next){
	console.log(err.stack);
	res.status(500);
	res.send('500 - Server Error');
});

// listen
app.listen(port, function(){
	console.log(' Pigeon is running on port: ' + port);
});
