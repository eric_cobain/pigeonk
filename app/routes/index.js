// routes index.js

module.exports = function(app, router){
	
	router.route('/')
	.get(function(req, res){
		res.render('index', {
			"name": " pigonk something "
			});
	});
	
	router.route('/cooking')
	.get(function(req, res){
		res.send('cooking');
	});

	router.route('/eating-out')
	.get(function(req, res){
		res.send('eating out');
	});

	router.route('/takeout')
	.get(function(req, res){
		res.send('takeout');
	});

	app.use('/', router);
}
