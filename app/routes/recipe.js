// recipe api

var Recipe = require('../models/recipe');

module.exports = function(app, router){
	
	router.route('/recipes')
	.get(function(req, res){
		Recipe.find(function(err, recipes){
			if(err)
				throw(err)
			res.json(recipes);
		});
	})
	.post(function(req, res){
		var recipe = new Recipe();
		recipe.name = req.body.name;
		recipe.description = req.body.description;
		recipe.save(function(err) {
			if(err)
				throw(err)
			res.json({ message: "New Recipe: '" + recipe.name + "'.", recipe: recipe });
		});	
	});
	
	
	app.use('/api', router);
};
