//recipe model

var mongoose = require('mongoose');

var recipeSchema = mongoose.Schema({
	name: String,
	description: String,
	final_images: [String],
});

module.exports = mongoose.model('Recipe', recipeSchema);
